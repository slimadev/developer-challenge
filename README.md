# Developer-Challenge
> Cliente REST que nos permite obter e visualizar as informações de propriedades dos países presentes na API (https://restcountries.eu).

## Como Rodar
```bash
# Obrigatiamente que tenha Nodejs a rodar no seu computador
# Instalar Dependencias apartir da linha de comando dentro de projecto
npm install

# Rodar a Aplicacao
npm start

#  Cliente roda em http://localhost:3000


## Main Technologies

Lado Cliente 
- [ ] ReactJs
- [ ] Ant Design

Bibliotecas Usadas
 - [ ] axios
 - [ ] export-from-json

## APIs

### Endpoints

GET
- https://restcountries.eu/rest/v2/all