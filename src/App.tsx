import React from 'react';
import './App.css';
import Country  from '../src/views/country';
import { Layout, Menu, Breadcrumb } from 'antd';
const { Header, Content, Footer } = Layout;

function App() {
  return (
    <div className="App">
      <Layout>
        <Header style={{ position: 'fixed', zIndex: 1, width: '100%' }}>
        <div className="logo" />
        <Menu theme="dark" mode="horizontal" defaultSelectedKeys={['1']}>
            <Menu.Item key="1">Paises</Menu.Item>
        </Menu>
        </Header>
        <Content className="site-layout" style={{ padding: '0 50px', marginTop: 64 }}>
        <Breadcrumb style={{ margin: '16px 0' }}>
            <Breadcrumb.Item>Home</Breadcrumb.Item>
            <Breadcrumb.Item>Paises</Breadcrumb.Item>
            <Breadcrumb.Item>Lista</Breadcrumb.Item>
        </Breadcrumb>
        <div className="site-layout-background" style={{ padding: 24, minHeight: '80vh' }}>
            <Country/>
        </div>
        </Content>
        <Footer style={{ textAlign: 'center' }}>Simone Lima ©2020 </Footer>
    </Layout>
    </div>
  );
}

export default App;
