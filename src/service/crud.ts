import axiosInstance from './api';

export async function create(params: any, payload: any = {}) {
  return await axiosInstance.post(params, payload);
}

export async function update(params: any, payload: any = {}) {
  return await axiosInstance.put(params, payload);
}

export async function select(params: any) {
  const res = await axiosInstance.get(params);
  return res.data;
}

export async function show(params: any, payload: any = {}) {
  return await axiosInstance.get(params, payload);
}