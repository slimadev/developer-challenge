import { select } from './crud';


export async function query() {
    return await select('/all');
}