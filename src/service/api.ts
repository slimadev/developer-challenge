import { notification } from 'antd';
import axios from 'axios';

function getFormattedMessage(status: any) {
  return 'Ocorreu um erro.'.concat(status);
}

export function isError(status: any): boolean {
  if (status >= 400 && status <= 500) {
    return true;
  }
  return false;
}

export function isSuccess(status: any): boolean {
  if (status >= 200 && status < 300) {
    return true;
  }
  return false;
}

const errorHandler = (error: { response: Response }): Response => {
  console.log('error',error)
  const { response } = error;
  if (response && response.status) {
    const errorText = getFormattedMessage(response.status) || response.statusText;
    const { status } = response;

    notification.error({
      message: `Error ${status}`,
      description: errorText,
    });
  } else if (!response) {
    notification.error({
      description: "Ocorreu um erro Inesperado",
      message: 'Error:',
    });
  }
  return response;
};

const axiosInstance = axios.create({
  baseURL: 'https://restcountries.eu/rest/v2/',
});

const successHandler = (response: any) => response;

axiosInstance.interceptors.response.use(
  response => successHandler(response),
  error => errorHandler(error),
);

axiosInstance.interceptors.request.use(
  config => {
    return config;
  },
  error => {
    Promise.reject(error);
  },
);

export default axiosInstance;