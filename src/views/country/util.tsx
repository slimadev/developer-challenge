import React from 'react';
import { Avatar } from 'antd';

export interface CountryModel {
    name:string;
    alpha2Code:string;
    capital:string;
    region:string;
    subregion:string;
    population:number;
    area:number;
    timezones:string[];
    nativeName:number;
    flag:string;
  }

  
  export const COLUMNS  = ()=>{
      return [
          {
            title:'Bandeira',
            dataIndex:'flag',
            key:'flag',
            fixed: false,
            width: 100,
            render: (record:any) => (
                <Avatar src={record} />
            )
        },
        {
            title:'Nome',
            dataIndex:'name',
            key:'name'
        },
        {
            title:'Capital',
            dataIndex:'capital',
            key:'capital'
        },
        {
            title:'Região',
            dataIndex:'region',
            key:'region'
        },
        {
            title:'Sub-Região',
            dataIndex:'subregion',
            key:'subregion'
        },
        {
            title:'População',
            dataIndex:'population',
            key:'population'
        },
        {
            title:'área',
            dataIndex:'area',
            key:'area'
        },
        {
            title:'Nome Nativo',
            dataIndex:'nativeName',
            key:'nativeName',
        },
        {
            title:'Fuso Horário',
            dataIndex:'timezones',
            key:'timezones',
            render: (record:any) => (
                record[0]
            )
        },
        
      ]
  }
    
  export const dataToExport  = function (responseData:any[]):CountryModel[] {
        return responseData.map(data=>{
            return {
                name:data.name,
                alpha2Code:data.alpha2Code,
                capital:data.capital,
                region:data.region,
                subregion:data.subregion,
                population:data.population,
                area:data.area,
                timezones:data.timezones[0],
                nativeName:data.nativeName,
                flag:data.flag
            }
        })
  }


  export const csvOptions = { 
    fieldSeparator: ',',
    quoteStrings: '"',
    decimalSeparator: '.',
    showLabels: true, 
    showTitle: true,
    title: 'Lista de Paises',
    useTextFile: false,
    useBom: true,
   // useKeysAsHeaders: true,
    headers: ['Bandeira', 'Nome', 'Região', 'Sub-Região', 'População', 'área', 'Nome Nativo', 'Fuso Horário'] 
  };

  export const CSV_TYPE = 'csv'
  export const XLS_TYPE = 'xls'
  export const XML_TYPE = 'txt'

