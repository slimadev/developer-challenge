import React, { useState, useEffect } from 'react';
import './index.css';
import { Card, Spin, Alert, Table, Menu, Dropdown,} from 'antd';
import { FilePdfOutlined, FileExcelOutlined, FileWordOutlined,  } from '@ant-design/icons';
import { query as  queryAllCountries} from '../../service/country';
import { CountryModel, COLUMNS, dataToExport, CSV_TYPE, XLS_TYPE, XML_TYPE} from './util'
import exportFromJSON from 'export-from-json'

function Country() {
    const [refresh, setRefresh] = useState<boolean>(false);
    const [isLoadingData, setLoadingData] = useState<boolean>(false);
    const [countries, setCountries] = useState<CountryModel[]>([]);

    useEffect(()=>{
        fetchCountries()
    },[refresh])

    const fetchCountries = async () =>{
        setLoadingData(true)
        const responseData = await queryAllCountries()
        const data = await dataToExport(responseData)
        setCountries(data)
        setLoadingData(false)
    }

    const handleMenuClick = (e:any) =>{
        console.log('click', e.key);
        const fileName = 'paises'
        const data = JSON.stringify(countries)
        if(Number(e.key) === 1){
            exportFromJSON({ data, fileName, exportType: XLS_TYPE})
        }else if(Number(e.key) === 2){
            exportFromJSON({ data, fileName, exportType: CSV_TYPE})
        }else{
            exportFromJSON({ data, fileName, exportType: XML_TYPE})
        }
        if(Number(e.key) === 10){
          setRefresh(false)
        }
      }

    const menu = (
        <Menu onClick={handleMenuClick}>
          <Menu.Item key="1" icon={<FileExcelOutlined />}>
            XLS
          </Menu.Item>
          <Menu.Item key="2" icon={<FilePdfOutlined />}>
            CSV
          </Menu.Item>
          <Menu.Item key="3" icon={<FileWordOutlined />}>
          TXT
          </Menu.Item>
        </Menu>
      );

    const COUNTRY_COLUMNS = COLUMNS()

  return (
    <Card type="inner" title="Lista de Paises" extra={<Dropdown.Button type="primary" onClick={()=>{}} overlay={menu}>
    Exportar para
  </Dropdown.Button>}>
       {    isLoadingData ? 
            <Spin tip="Loading...">
                    <Alert
                    message="Agurde por favor..."
                    description="Carregado Dados de Paises."
                    type="info"
                    />
                </Spin>
            :
            <Table columns={COUNTRY_COLUMNS} dataSource={countries}></Table>
        }
    </Card>
  );
}

export default Country;
